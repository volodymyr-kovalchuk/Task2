package ua.com.sourceit.kovalchuk;

public class SubTask2 {
    public static void main(String[] args) {
        System.out.println(substrCount("Good Golly Miss Molly", "ll", 7, 10));
        System.out.println(substrCount("Good Golly Miss Molly", "ll", 7, 14));
    }

    public static int substrCount(String input, String needle, int offset, int length) {
        if (input.length() < offset + length) return 0;

        int counter = 0;
        int startIndex = offset;
        int index = input.substring(startIndex, offset + length).indexOf(needle);

        while (index != -1) {
            counter++;
            startIndex += index + needle.length();
            index = input.substring(startIndex, offset + length).indexOf(needle);
        }
        return counter;
    }
}
