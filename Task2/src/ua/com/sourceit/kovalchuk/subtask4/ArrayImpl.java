package ua.com.sourceit.kovalchuk.subtask4;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayImpl implements Array {
    public static final int INITIAL_CAPACITY_DEFAULT_SIZE = 10;
    private int arrayLength;
    private final int initialCapacity;
    private int arrayListSize;
    private Object[] array;

    public static void main(String[] args) {
        ArrayImpl arr = new ArrayImpl();
        arr.add(0);
        System.out.println("Integer 0 added: " + arr);
        arr.add(1);
        System.out.println("Integer 1 added: " + arr);
        arr.add(2);
        System.out.println("Integer 2 added: " + arr);
        arr.add(3);
        System.out.println("Integer 3 added: " + arr);
        arr.add(4);
        System.out.println("Integer 4 added: " + arr);

        arr.set(3, 333);
        System.out.println("Integer with index 3 changed to 333: " + arr);

        arr.remove(2);
        System.out.println("Integer with index 2 removed: " + arr);

        System.out.println("Index of integer 333 is: " + arr.indexOf(333));

        System.out.println("Size of this ArrayImpl is: " + arr.size());

        String printInfo = arr.toString();
        System.out.println("An explicit call to the toString method: " + printInfo);

        System.out.println("    ArrayImpl before iteration: " + arr);
        System.out.print("    Iterate, print every iterated element and delete if current element = 333: ");
        Iterator<Object> iterator = arr.iterator();
        while (iterator.hasNext()) {
            Object element = iterator.next();
            if (element.equals(333)) iterator.remove();
            System.out.print(element);
            if (iterator.hasNext()) System.out.print(", ");
        }
        System.out.println();
        System.out.println("    ArrayImpl after iteration: " + arr);

        arr.clear();
        System.out.println("ArrayImpl array cleared: " + arr);
    }

    public ArrayImpl() {
        initialCapacity = INITIAL_CAPACITY_DEFAULT_SIZE;
        arrayLength = initialCapacity;
        arrayListSize = 0;
        array = new Object[arrayLength];
    }

    public ArrayImpl(int initialCapacity) {
        this.initialCapacity = initialCapacity;
        arrayLength = this.initialCapacity;
        arrayListSize = 0;
        array = new Object[arrayLength];
    }

    class IteratorImpl implements Iterator<Object> {
        private int cursor = 0;
        private int lastRet = 0;


        @Override
        public boolean hasNext() {
            return cursor < arrayListSize && array[cursor] != null;
        }

        @Override
        public Object next() {
            lastRet = cursor;
            cursor++;
            return array[lastRet];
        }

        @Override
        public void remove() {
            ArrayImpl.this.remove(lastRet);
            cursor = lastRet;
        }
    }

    @Override
    public void add(Object element) {
        checkAndIncreaseInnerArray(); // check and if needed increase inner array of ArrayList
        array[arrayListSize] = element;
        arrayListSize++;
    }

    private void checkAndIncreaseInnerArray() {
        if (arrayListSize == arrayLength) {
            arrayLength += initialCapacity;
            System.arraycopy(array, 0, array, 0, arrayLength);
        }
    }

    @Override
    public void set(int index, Object element) {
        if (index < 0 || index >= arrayListSize) throw new ArrayIndexOutOfBoundsException();

        array[index] = element;
    }

    @Override
    public int indexOf(Object element) {
        for (int i = 0; i < arrayListSize; i++) {
            if (array[i].equals(element)) return i;
        }
        return -1;
    }

    @Override
    public void remove(int index) {
        if (isEmpty()) throw new NoSuchElementException();
        if (index < 0 || index >= arrayListSize) throw new ArrayIndexOutOfBoundsException();

        Object[] newArray;
        arrayListSize--;

        /* Check if decreasing of inner array needed */
        if (arrayListSize != arrayLength - initialCapacity) {
            newArray = new Object[arrayListSize];
        } else {
            arrayLength -= initialCapacity;
            newArray = new Object[arrayLength];
        }

        System.arraycopy(array, 0, newArray, 0, index);
        System.arraycopy(array, index + 1, newArray, index, arrayListSize - index);

        array = new Object[newArray.length];
        System.arraycopy(newArray, 0, array, 0, newArray.length);
    }

    public boolean isEmpty() {
        return arrayListSize == 0;
    }

    @Override
    public void clear() {
        arrayLength = initialCapacity;
        arrayListSize = 0;
        array = new Object[arrayLength];
    }

    @Override
    public int size() {
        return arrayListSize;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("[");
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                if (i != 0) result.append(", ");
                result.append(array[i]);
            }
        }
        result.append("]");
        return result.toString();
    }

    @Override
    public Iterator<Object> iterator() {
        return new IteratorImpl();
    }
}
