package ua.com.sourceit.kovalchuk;

import java.util.Random;
import java.util.Scanner;

public class SubTask3 {
    public static void main(String[] args) {
        int computerNumber = new Random().nextInt(100) + 1;

        while (true) {
            System.out.println("Please enter integer number from 1 to 100:");
            int humanNumber = getNumber();

            if (humanNumber > 100 || humanNumber < 1) {
                System.out.println("The entered number is not in the integer range from 1 to 100");
            } else {
                if (humanNumber == computerNumber) {
                    System.out.println("Congratulation! You win!");
                    break;
                } else if (humanNumber > computerNumber) {
                    System.out.println("The entered number is more than the computer guessed");
                } else System.out.println("The entered number is less than the computer guessed");
            }
        }
    }

    private static int getNumber() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            String line = sc.nextLine();
            try {
                return Integer.parseInt(line);
            } catch (NumberFormatException e) {
                System.out.println("The entered number is not integer, please try again:");
            }
        }
    }
}
