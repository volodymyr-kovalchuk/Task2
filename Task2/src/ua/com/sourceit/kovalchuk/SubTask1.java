package ua.com.sourceit.kovalchuk;

import java.util.Stack;

public class SubTask1 {
    public static void main(String[] args) {
        System.out.println(testString(""));
        System.out.println(testString("fghfg"));
        System.out.println(testString("]"));
        System.out.println(testString("("));
        System.out.println(testString("()"));
        System.out.println(testString("()[]"));
        System.out.println(testString("()[(]"));
        System.out.println(testString("()[)]"));
        System.out.println(testString("isu[syv(stc]ts(crs))cs”"));
        System.out.println(testString("isu[syv](stcts(crs))cs"));
    }

    public static boolean testString(String line) {
        int counter = 0;
        Stack<Character> brackets = new Stack<>();
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == '(' || line.charAt(i) == '[') {
                brackets.push(line.charAt(i));
                counter++;
            }

            if ((line.charAt(i) == ')' || line.charAt(i) == ']') && brackets.isEmpty()) {
                return false;
            } else if ((line.charAt(i) == ')' && brackets.pop() != '(')
                    || (line.charAt(i) == ']' && brackets.pop() != '[')
            ) return false;
        }
        return counter != 0 && brackets.isEmpty();
    }
}